export default {
  data () {
    return {
      groups: {
        'Dimensions': {
          legend: 'Dimensions',
          open: false,
          fields: {
            'width': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Width',
              model: 'width',
              css: '{{ widget }} { width: {{ value }}; }',
              styleClasses: 'half'
            },
            'max-width': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Max Width',
              model: 'max-width',
              css: '{{ widget }} { max-width: {{ value }}; }',
              styleClasses: 'half last'
            },
            'height': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Height',
              model: 'height',
              css: '{{ widget }} { height: {{ value }}; }',
              styleClasses: 'half'
            },
            'max-height': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Max Height',
              model: 'max-height',
              styleClasses: 'half last'
            },
            'margin-left': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Margin Left',
              model: 'margin-left',
              styleClasses: 'half'
            },
            'margin-right': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Margin Right',
              model: 'margin-right',
              styleClasses: 'half last'
            },
            'margin-top': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Margin Top',
              model: 'margin-top',
              styleClasses: 'half'
            },
            'margin-bottom': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Margin Bottom',
              model: 'margin-bottom',
              styleClasses: 'half last'
            },
            'padding-left': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Padding Left',
              model: 'padding-left',
              styleClasses: 'half',
              min: '0'
            },
            'padding-right': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Padding Right',
              model: 'padding-right',
              styleClasses: 'half last',
              min: '0'
            },
            'padding-top': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Padding Top',
              model: 'padding-top',
              styleClasses: 'half',
              min: '0'
            },
            'padding-bottom': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Padding Bottom',
              model: 'padding-bottom',
              styleClasses: 'half last',
              min: '0'
            }
          }
        }
      }
    }
  }
}
