import Vue from 'vue'
import MediumEditor from 'medium-editor'

export default {
  data () {
    return {
      editor: null
    }
  },
  mounted () {
    // this.initEditor()
  },
  methods: {
    initEditor () {
      if (this.$store.state.iframePreview) {
        if (this.editor) {
          this.editor.destroy()
        }
        var _this = this
        Vue.nextTick(function () {
          _this.editor = new MediumEditor(_this.$el, {
            contentWindow: _this.$store.state.iframePreview.$el.contentWindow,
            ownerDocument: _this.$store.state.iframePreview.$el.contentWindow.document,
            elementsContainer: _this.$store.state.iframePreview.$el.contentWindow.document.body,
            disableReturn: false,
            disableDoubleReturn: false,
            toolbar: {
              buttons: [
                'bold',
                'italic',
                'underline',
                'anchor',
                'h2',
                'h3',
                'quote'
              ]
            }
          })
        })
      }
    }
  }
  // render (h) {
  //   if (!this.templateRender) {
  //     return h('div', 'Loading...')
  //   } else {
  //     return this.templateRender()
  //   }
  // },
  // methods: {
  //   textInput (event) {
  //     this.model.content = event.target.innerHTML
  //   }
  // },
  // watch: {
  //   'model.content': {
  //     immediate: true,
  //     handler () {
  //       var compiled = Vue.compile('<div @input="textInput">Before link text <el :model="{type: \'link-element\', text: \'testlink\'}"></el>Testingmoo:' + this.model.content + '</div>')
  //       this.templateRender = compiled.render
  //       this.$options.staticRenderFns = []
  //       this._staticTrees = []
  //       for (var i in compiled.staticRenderFns) {
  //         this.$options.staticRenderFns.push(compiled.staticRenderFns[i])
  //       }
  //     }
  //   }
  // },
  // data () {
  //   return {
  //     templateRender: null
  //   }
  // },
}
