export default {
  data () {
    return {
      groups: {
        'Typography': {
          legend: 'Typography',
          open: false,
          fields: {
            'font-family': {
              type: 'select',
              label: 'Font Family',
              model: 'font-family',
              selectOptions: {
                noneSelectedText: ' '
              },
              values: function () {
                return [
                  { id: 'arial', name: 'Arial' },
                  { id: 'helvetica', name: 'Helvetica' }
                ]
              }
            },
            'font-size': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Font Size',
              model: 'font-size',
              styleClasses: 'half'
            },
            'font-weight': {
              type: 'select',
              label: 'Font Weight',
              model: 'font-weight',
              selectOptions: {
                noneSelectedText: ' '
              },
              styleClasses: 'half last',
              values: function () {
                return [
                  { id: '100', name: '100 Thin' },
                  { id: '200', name: '200 Extra Light' },
                  { id: '300', name: '300 Light' },
                  { id: '400', name: '400 Normal' },
                  { id: '500', name: '500 Medium' },
                  { id: '600', name: '600 Semi Bold' },
                  { id: '700', name: '700 Bold' },
                  { id: '800', name: '800 Extra Bold' },
                  { id: '900', name: '900 Black' }
                ]
              }
            },
            'text-transform': {
              type: 'select',
              label: 'Text Transform',
              model: 'text-transform',
              selectOptions: {
                noneSelectedText: ' '
              },
              styleClasses: 'half',
              values: function () {
                return [
                  { id: 'none', name: 'None' },
                  { id: 'uppercase', name: 'Uppercase' },
                  { id: 'lowercase', name: 'LowerCase' },
                  { id: 'capitalize', name: 'Capitalize' }
                ]
              }
            },
            'line-height': {
              type: 'dimension',
              defaultUnit: 'px',
              label: 'Line Height',
              model: 'line-height',
              css: '{{ widget }} { line-height: {{ value }}; }',
              styleClasses: 'half last'
            },
            'color': {
              type: 'colorPicker',
              label: 'Color',
              model: 'color',
              default: { hex: '#ffffff' },
              styleClasses: 'one-quarter'
            },
            'text-align': {
              type: 'radioIcons',
              label: 'Text Align',
              model: 'text-align',
              values: function () {
                return [
                  { value: '', icon: this.$constants.feather['x'].toSvg() },
                  { value: 'left', icon: this.$constants.feather['align-left'].toSvg() },
                  { value: 'center', icon: this.$constants.feather['align-center'].toSvg() },
                  { value: 'right', icon: this.$constants.feather['align-right'].toSvg() },
                  { value: 'justify', icon: this.$constants.feather['align-justify'].toSvg() }
                ]
              },
              styleClasses: 'three-quarters last'
            }
          }
        }
      }
    }
  }
}
