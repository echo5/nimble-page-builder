import utils from '../utils'

export default {
  groups (toVal, fromVal) {
    var groups = utils.mergeDeep(fromVal, toVal)
    return groups
  }
}
