import Vue from 'vue'
import store from '../store'

export default {
  props: {
    model: Object,
    parentKey: null,
    elementKey: null,
    parentElement: null
  },
  groups: {
    'Selectors': {
      legend: 'Selectors',
      open: true,
      fields: {
        'classes': {
          type: 'vueMultiSelect',
          model: 'classes',
          label: 'Classes',
          placeholder: 'Element classes',
          tagPlaceholder: 'Add new class',
          required: true,
          default: [],
          selectOptions: {
            hideSelected: true,
            multiple: true,
            searchable: true,
            taggable: true,
            closeOnSelect: true,
            allowEmpty: true,
            internalSearch: true,
            onNewTag: function (newTag, id, options, value) {
              store.commit('addRuleSet', '.' + newTag)
              if (!value) {
                value = []
              }
              value.push(newTag)
            }
          },
          onChanged: function (model, newVal, oldVal, field) {
            Vue.nextTick(function () {
              store.commit('setCurrentSelector', '.' + newVal.join('.'))
            })
          },
          values: function () {
            return store.getters.cssSelectors
          }
        },
        'id': {
          type: 'input',
          inputType: 'text',
          label: 'ID',
          model: 'id'
        }
      }
    }
  }
}
