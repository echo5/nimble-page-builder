const undoRedo = store => {
  // called when the store is initialized
  store.subscribe((mutation, state) => {
    console.log(state)
    // called after every mutation.
    // The mutation comes in the format of `{ type, payload }`.
  })
}
export default undoRedo
