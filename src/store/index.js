import Vue from 'vue'
import Vuex from 'vuex'
import VueFormGenerator from 'vue-form-generator'
import utils from '../utils'
import Specificity from 'specificity'
import shortid from 'shortid'
// import undoRedo from './undoRedo'
Vue.use(Vuex)

const store = new Vuex.Store({
  // plugins: [undoRedo],
  state: {
    elements: {
      'root': {
        children: []
      }
    },
    ruleSets: null,
    showElementModal: false,
    styleSidebarClosed: false,
    assetManagerOpen: false,
    currentModel: null,
    currentProperty: null,
    elementMenuOpen: false,
    importModalOpen: false,
    currentPseudoClass: null,
    currentSelector: null,
    currentElementKey: null,
    hoveredElementKey: null,
    openElementTreeItems: {},
    openRuleSetTreeItems: {},
    highlightedElements: [],
    showContextMenu: false,
    currentTab: 'styles',
    contextMenuPosition: {
      top: '0px',
      left: '0px'
    },
    iframePreview: null,
    styleNode: null,
    currentMediaQuery: null,
    isExporting: false,
    paddingMode: false,
    marginMode: false,
    dimensionsMode: false,
    inlineEditorMode: false
  },
  getters: {
    ruleSets: state => {
      return state.ruleSets
    },
    ruleSetBySelector: (state, selector) => {
      return state.ruleSets.find(function (ruleSet) {
        return ruleSet.selectors && ruleSet.selectors[0] === selector
      })
    },
    currentRuleSet: state => {
      if (state.currentSelector || state.currentSelector === '') {
        if (state.currentMediaQuery && state.currentMediaQuery.query) {
          store.commit('addMediaQuery', state.currentMediaQuery.query)
          store.commit('addNestedRule', [state.currentMediaQuery.query, state.currentSelector])
          var mediaQuery = state.ruleSets.find(function (ruleSet) {
            return ruleSet.value && ruleSet.value[0] === state.currentMediaQuery.query
          })
          return mediaQuery.nestedRules.find(function (nestedRuleSet) {
            return nestedRuleSet.selectors && nestedRuleSet.selectors[0] === state.currentSelector
          })
        } else {
          return state.ruleSets.find(function (ruleSet) {
            return ruleSet.selectors && ruleSet.selectors[0] === state.currentSelector
          })
        }
      }
    },
    currentElementKey: state => {
      return state.currentElementKey
    },
    currentElementNode: state => {
      return state.iframePreview.$el.contentWindow.document.querySelector('[data-vb-element-key="' + state.currentElementKey + '"]')
    },
    currentElementModel: state => {
      return state.elements[state.currentElementKey]
    },
    hoveredElementNode: state => {
      return state.iframePreview.$el.contentWindow.document.querySelector('[data-vb-element-key="' + state.hoveredElementKey + '"]')
    },
    hoveredElementModel: state => {
      return state.elements[state.hoveredElementKey]
    },
    currentElementComponent: state => {
      if (state.currentElementKey) {
        return Vue.options.components[state.elements[state.currentElementKey].type].options
      }
    },
    iframePreview: state => {
      return state.iframePreview
    },
    styleNode: state => {
      return state.styleNode
    },
    cssSelectors: state => {
      var options = []
      store.state.ruleSets.forEach(function (ruleSet) {
        if (ruleSet.selectors) {
          var selector = ruleSet.selectors[0]
          if (!selector.startsWith('(')) {
            var selectors = selector.split('.')
            selectors.forEach(function (cssClass) {
              // Remove psuedos and IDs
              cssClass = cssClass.split(':')[0]
              cssClass = cssClass.split('#')[0]
              cssClass = cssClass.trim()
              if (!options.includes(cssClass) && cssClass !== '') {
                options.push(cssClass)
              }
            })
          }
        }
      })
      return options
    },
    currentElementSelectorMatches: (state) => {
      var selectors = []
      if (store.getters.currentElementNode) {
        var el = store.getters.currentElementNode
        store.state.ruleSets.forEach(function (ruleSet) {
          if (ruleSet.type === 'rule' && ruleSet.selectors[0]) {
            if (el && el.matches(ruleSet.selectors[0])) {
              selectors.push(ruleSet.selectors[0])
            }
          }
        })
      }

      return selectors.sort(Specificity.compare)
    }
  },

  actions: {
    editElement: ({ commit, state }, [elementKey, classCycleCounter]) => {
      commit('setCurrentElementKey', elementKey)
      commit('closeElementModal')
      commit('openElementTreeItem', elementKey)

      var selector = '.' + state.elements[elementKey].classes.length > 0 ? '.' + state.elements[elementKey].classes.join('.') : ''
      selector = utils.getSimilarSelector(selector, state.ruleSets)
      commit('addRuleSet', selector)

      Vue.nextTick(function () {
        var selectorMatches = store.getters.currentElementSelectorMatches
        var slice = (classCycleCounter % selectorMatches.length) * -1
        commit('setCurrentSelector', selectorMatches.sort(Specificity.compare).slice(slice)[0])
      })
    },
    importHtml: ({ commit, dispatch }, [htmlString, elementTypes]) => {
      var template = document.createElement('div')
      var html = htmlString.trim()
      template.innerHTML = html
      if (template.childNodes) {
        // console.log(template.child)
        dispatch('parseChildNodes', [template.childNodes, 'root', elementTypes])
      }
    },
    parseChildNodes: ({ commit, dispatch }, [nodes, parentKey, elementTypes]) => {
      for (var i = 0; i < nodes.length; i++) {
        var node = nodes[i]
        var elementKey = shortid.generate()
        var elementFromNode = utils.getElementFromNode(node, elementTypes)
        commit('createElement', [
          elementKey,
          elementFromNode.type,
          elementFromNode.properties
        ])
        commit('addChildElement', [parentKey, elementKey])
        dispatch('parseChildNodes', [node.childNodes, elementKey, elementTypes])
      }
    }

  },

  mutations: {

    emptyState () {
      this.replaceState({
        elements: window.elements,
        ruleSets: null,
        showElementModal: false,
        styleSidebarClosed: false,
        elementMenuOpen: false,
        importModalOpen: false,
        currentSelector: null,
        currentPseudoClass: null,
        currentElementKey: null,
        currentElement: null,
        hoveredElement: null,
        currentElementNode: null,
        openElementTreeItems: {},
        openRuleSetTreeItems: {},
        highlightedElements: [],
        showContextMenu: false,
        currentTab: 'styles',
        contextMenuPosition: {
          top: '0px',
          left: '0px'
        },
        iframePreview: null,
        styleNode: null,
        currentMediaQuery: null,
        isExporting: false,
        paddingMode: false,
        marginMode: false,
        dimensionsMode: false,
        positionMode: false
      })
    },

    setElements: (state, elements) => {
      state.elements = elements
    },

    setCurrentModelProperty: (state, [model, property]) => {
      state.currentModel = model
      state.currentProperty = property
    },

    setCurrentElementKey: (state, elementKey) => {
      state.currentElementKey = elementKey
    },

    openElementTreeItem: (state, elementKey) => {
      Vue.set(state.openElementTreeItems, elementKey, true)
      var parent = utils.getParent(state.elements, elementKey, 'children')
      while (parent) {
        Vue.set(state.openElementTreeItems, parent, true)
        parent = utils.getParent(state.elements, parent, 'children')
      }
    },

    addElement: (state, [key, element]) => {
      Vue.set(state.elements, key, element)
    },

    createElement: (state, [id, type, properties]) => {
      var groups = Vue.options.components[type].options.groups
      var defaultProperties = {
        type: type,
        children: [],
        classes: []
      }
      var mergedProperties = Object.assign(defaultProperties, properties)
      var newElement = VueFormGenerator.schema.createDefaultObject({ fields: utils.getAllFieldsFromGroups(groups) }, mergedProperties)
      store.commit('addElement', [id, newElement])
    },

    deleteElement: (state, id) => {
      if (state.currentElementKey === id) {
        state.currentElementKey = null
        state.currentElement = null
      }
      // Delete children
      if (state.elements[id].children) {
        for (var i = 0; i < state.elements[id].children.length; i++) {
          store.commit('deleteElement', state.elements[id].children[i])
        }
      }
      // Delete from parent
      var parent = utils.getParent(state.elements, id, 'children')
      if (parent) {
        Vue.delete(state.elements[parent]['children'], state.elements[parent]['children'].indexOf(id))
      }
      // Delete element
      Vue.delete(state.elements, id)
    },

    addChildElement: (state, [parentKey, childKey, index]) => {
      if (index || index === 0) {
        state.elements[parentKey].children.splice(index, 0, childKey)
      } else {
        state.elements[parentKey].children.push(childKey)
      }
    },

    removeChildElement: (state, [childKey, parentKey]) => {
      var index = state.elements[parentKey].children.indexOf(childKey)
      state.elements[parentKey].children.splice(index, 1)
    },

    setCurrentElement: (state, element) => {
      state.currentElement = element
    },

    setHoveredElementKey: (state, key) => {
      state.hoveredElementKey = key
    },

    setCurrentMediaQuery: (state, query) => {
      state.currentMediaQuery = query
    },

    setCurrentSelector: (state, selector) => {
      // Force refresh on item
      var element = state.currentElement
      state.currentElement = null
      state.currentElement = element

      // Set temp to retroactively define if none set
      if (!selector || selector === '' || selector.length < 1) {
        selector = ''
      }
      store.commit('addRuleSet', selector)

      state.currentSelector = selector
    },

    renameRuleSet: (state, [oldSelector, newSelector]) => {
      if (oldSelector !== newSelector) {
        var ruleSet = state.ruleSets.find(function (ruleSet) {
          return ruleSet.selectors && ruleSet.selectors[0] === oldSelector
        })
        Vue.set(ruleSet.selectors, 0, newSelector)
      }
    },

    deleteRuleSet: (state, ruleSetName) => {
      Vue.delete(state.ruleSets, ruleSetName)
    },

    closeModal: (state) => {
      state.showElementModal = false
    },

    openElementMenu: (state) => {
      state.elementMenuOpen = true
    },

    openElementModal: (state) => {
      state.showElementModal = true
    },

    closeElementMenu: (state) => {
      state.elementMenuOpen = false
    },

    closeElementModal: (state) => {
      state.elementModalOpen = false
    },

    toggleElementMenu: (state) => {
      if (state.elementMenuOpen) {
        state.elementMenuOpen = false
      } else {
        state.elementMenuOpen = true
      }
    },

    toggleAssetManager: (state) => {
      if (state.assetManagerOpen) {
        state.assetManagerOpen = false
      } else {
        state.assetManagerOpen = true
      }
    },

    toggleImportModal: (state) => {
      if (state.importModalOpen) {
        state.importModalOpen = false
      } else {
        state.importModalOpen = true
      }
    },

    changeTab: (state, label) => {
      state.currentTab = label
    },

    addRuleSet: (state, selector) => {
      var existingSelector = state.ruleSets.find(function (ruleSet) {
        return ruleSet.selectors && ruleSet.selectors[0] === selector
      })
      if (!existingSelector && (utils.isValidSelector(selector) || selector === '')) {
        state.ruleSets.push({
          type: 'rule',
          declarations: {},
          selectors: [selector]
        })
      }
    },

    addNestedRule: (state, [parent, selector]) => {
      var mediaQuery = state.ruleSets.find(function (ruleSet) {
        return ruleSet.value && ruleSet.value[0] === parent
      })
      var existingSelector = mediaQuery.nestedRules.find(function (ruleSet) {
        return ruleSet.selectors && ruleSet.selectors[0] === selector
      })
      if (!existingSelector) {
        mediaQuery.nestedRules.push({
          type: 'rule',
          selectors: [selector],
          declarations: {}
        })
      }
    },

    addMediaQuery: (state, query) => {
      var existingMediaQuery = state.ruleSets.find(function (ruleSet) {
        return ruleSet.value && ruleSet.value[0] === query
      })
      if (!existingMediaQuery) {
        state.ruleSets.push({
          type: '@media',
          value: [query],
          nestedRules: []
        })
      }
    },

    setRuleSets: (state, ruleSets) => {
      state.ruleSets = ruleSets
    },

    updateRuleSet: (state, [selector, property, value]) => {
      var ruleSet = state.ruleSets.find(function (ruleSet) {
        return ruleSet.selectors && ruleSet.selectors[0] === selector
      })
      Vue.set(ruleSet.declarations, property, value)
    },

    setIframePreview: (state, iframe) => {
      state.iframePreview = iframe
    },

    setStyleNode: (state, node) => {
      state.styleNode = node
    },

    openContextMenu: (state, event) => {
      state.contextMenuPosition.top = event.y
      state.contextMenuPosition.left = event.x
      state.showContextMenu = true
    },

    closeContextMenu: (state) => {
      state.showContextMenu = false
    },

    startExport: (state) => {
      state.isExporting = true
    },

    endExport: (state) => {
      state.isExporting = false
    },

    setPaddingMode: (state, value) => {
      state.paddingMode = value
    },

    setMarginMode: (state, value) => {
      state.marginMode = value
    },

    setDimensionsMode: (state, value) => {
      state.dimensionsMode = value
    },

    setPositionMode: (state, value) => {
      state.positionMode = value
    },

    setInlineEditorMode: (state, value) => {
      state.inlineEditorMode = value
    }

  }

})

export default store
