// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store'
import utils from './utils'
import DefaultOptions from './utils/options.js'

// Merging mixins
import MergeStrategy from './mixins/mergeStrategy'

// Third party
import Draggable from 'vuedraggable'
import VueFormGenerator from 'vue-form-generator'
import Multiselect from 'vue-multiselect'

// Components
import Element from './components/Element.vue'
import Editable from './components/Editable.vue'

// Fields
import fieldAsset from './fields/fieldAsset.vue'
import fieldDimension from './fields/fieldDimension.vue'
import fieldRadioIcons from './fields/fieldRadioIcons.vue'
import fieldColorPicker from './fields/fieldColorPicker.vue'
Vue.component('fieldAsset', fieldAsset)
Vue.component('fieldDimension', fieldDimension)
Vue.component('fieldRadioIcons', fieldRadioIcons)
Vue.component('fieldColorPicker', fieldColorPicker)
Vue.use(VueFormGenerator)

// Builder
Vue.component('draggable', Draggable)
Vue.component('vue-form-generator', VueFormGenerator.component)
Vue.component('multiselect', Multiselect)
Vue.component('el', Element)
Vue.component('editable', Editable)

// Merge
Vue.config.optionMergeStrategies.groups = MergeStrategy.groups

// Reusable items
Vue.prototype.$constants = {}
Vue.prototype.$constants.feather = require('feather-icons').icons
Vue.prototype.$utils = utils
Vue.prototype.$VueFormGenerator = VueFormGenerator
Vue.prototype.$config = DefaultOptions

Vue.config.productionTip = false

export default {
  /* eslint-disable no-new */
  init (selector, options = {}) {
    Vue.prototype.$config = Object.assign(Vue.prototype.$config, options)
    Object.keys(Vue.prototype.$config.elementTypes).forEach(function (value, key) {
      Vue.component(value, Vue.prototype.$config.elementTypes[value])
    })

    new Vue({
      el: selector,
      store,
      components: { App },
      template: '<App/>'
    })
  },
  setHtml (htmlString) {
    store.dispatch('importHtml', [htmlString, Vue.prototype.$config.elementTypes])
  },
  addWidgets (widgets) {
    for (var i = 0; i < widgets.length; i++) {
      Vue.prototype.$config.elementTypes[widgets[i].name] = widgets[i]
    }
  }
}
