const shortid = require('shortid')
const Vue = require('Vue')

export default {

  /**
   * Get element from node
   * @param {Object[]} node
   */
  getElementFromNode (node, elementTypes) {
    var type = 'div-element'
    var properties = {}
    for (var elementType in elementTypes) {
      var nodeProperties = elementTypes[elementType]
        .parseNode
        ? elementTypes[elementType].parseNode(node)
        : false
      if (nodeProperties) {
        properties = nodeProperties
        type = elementTypes[elementType].name
        break
      }
    }
    return {
      type: type,
      properties: properties
    }
  },

  /**
   * Get grid sizes in array
   */
  getGridPercentages (columns) {
    var sizes = []
    for (var i = 0; i < columns + 1; i++) {
      sizes[i] = {
        value: i,
        percentage: i / columns * 100
      }
    }
    return sizes
  },

  /**
   * Find closest value and index in array
   */
  getClosestNumber (array, targetValue) {
    var closestNumber = null
    var closestIndex = null
    array.forEach(function (element, index) {
      if (closestIndex == null || Math.abs(element.percentage - targetValue) < Math.abs(closestNumber - targetValue)) {
        closestNumber = element.percentage
        closestIndex = index
      }
    })
    return array[closestIndex].value
  },

  closest (array, number) {
    var curr = array[0]
    var diff = Math.abs(number - curr)
    for (var val = 0; val < array.length; val++) {
      var newdiff = Math.abs(number - array[val])
      if (newdiff < diff) {
        diff = newdiff
        curr = array[val]
      }
    }
    return curr
  },

  getAllFieldsFromGroups (groups) {
    var schema = {}
    Object.keys(groups).forEach(function (group) {
      schema = Object.assign({}, groups[group])
    })
    return schema['fields']
  },

  setPosition (element, top, left) {
    var largestHeight = window.innerHeight - element.offsetHeight - 25
    var largestWidth = window.innerWidth - element.offsetWidth - 25

    if (top > largestHeight) top = largestHeight

    if (left > largestWidth) left = largestWidth

    element.style.top = top + 'px'
    element.style.left = left + 'px'
  },

  dragStart (event) {
    event.preventDefault()
    if (event.button === 2 || event.which === 3) {
      // disable right click
      return false
    }
  },

  hasClass (element, className) {
    return element.className && new RegExp('(^|\\s)' + className + '(\\s|$)').test(element.className)
  },

  isObject (item) {
    return (item && typeof item === 'object' && !Array.isArray(item))
  },

  isValidSelector (selector) {
    var dummy = document.createElement('br')
    try {
      dummy.querySelector(selector)
    } catch (e) {
      return false
    }
    return true
  },

  mergeDeep (target, source) {
    let output = Object.assign({}, target)
    if (this.isObject(target) && this.isObject(source)) {
      Object.keys(source).forEach(key => {
        if (this.isObject(source[key])) {
          if (!(key in target)) {
            Object.assign(output, { [key]: source[key] })
          } else {
            output[key] = this.mergeDeep(target[key], source[key])
          }
        } else {
          Object.assign(output, { [key]: target[key] })
        }
      })
    }
    return output
  },

  getParent (object, key, children) {
    var parent = null
    Object.keys(object).forEach(function (item) {
      if (object[item][children] && object[item][children].includes(key)) {
        parent = item
      }
    })
    return parent
  },

  arrayToObject (array, keyField) {
    array.reduce((obj, item) => {
      obj[item[keyField]] = item
      return obj
    }, {})
  },

  ruleSetsArrayToObject (items) {
    var objects = {}
    for (var i = 0, length = items.length; i < length; i++) {
      // Rules
      if (items[i]['type'] === 'rule' && items[i]['selectors']) {
        // Split selectors if more than one
        if (items[i]['selectors'].length > 1) {
          var itemSelectors = items[i]['selectors']
          itemSelectors.forEach(function (itemSelector) {
            var newItem = JSON.parse(JSON.stringify(items[i]))
            objects[itemSelector] = newItem
            objects[itemSelector]['selectors'] = [itemSelector]
          })
        } else {
          objects[items[i]['selectors'][0]] = items[i]
        }
      } else if (items[i]['type'] === '@media') {
        // Media queries
        objects[items[i]['value']] = items[i]
        if (items[i]['nestedRules']) {
          objects[items[i]['value']]['nestedRules'] = this.ruleSetsArrayToObject(items[i]['nestedRules'])
        }
      }
    }
    return objects
  },

  nodesToJson (htmlString) {
    var template = document.createElement('div')
    var html = htmlString.trim()
    template.innerHTML = html

    var elements = {
      root: {
        children: []
      }
    }
    if (template.childNodes) {
      elements = this.parseChildNodes(elements, template.childNodes, 'root')
    }
    return elements
  },

  parseChildNodes (elements, nodes, parentKey) {
    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i]
      var elementKey = shortid.generate()
      console.log(this.$config)
      elements[elementKey] = {
        classes: node.className ? node.className.split(' ') : [],
        type: 'div-element',
        children: [],
        text: node.innerHTML
      }
      elements[parentKey].children.push(elementKey)
      elements = this.parseChildNodes(elements, node.childNodes, elementKey)
    }
    return elements
  },

  getNodeElementType (node) {
    Object.keys(Vue.prototype.$config.elementTypes).forEach(function (elementType) {
      if (Vue.prototype.$config.elementTypes[elementType].isType && Vue.prototype.$config.elementTypes[elementType].isType(node)) {
        return Vue.prototype.$config.elementTypes[elementType].name
      }
    })
  },

  parseCssSelectorString (selector) {
    var obj = { tags: [], classes: [], ids: [], attrs: [], pseudoClasses: [] }
    selector.split(/(?=\.)|(?=#)|(?=\[)|(?=:)/).forEach(function (token) {
      switch (token[0]) {
        case '#':
          obj.ids.push(token.slice(1))
          break
        case '.':
          obj.classes.push(token.slice(1))
          break
        case '[':
          obj.attrs.push(token.slice(1, -1).split('='))
          break
        case ':':
          obj.pseudoClasses.push(token.slice(1))
          break
        default:
          obj.tags.push(token)
          break
      }
    })
    return obj
  },

  permute (permutation) {
    var length = permutation.length
    var result = [permutation.slice()]
    var c = new Array(length).fill(0)
    var i = 1
    var k
    var p

    while (i < length) {
      if (c[i] < i) {
        k = i % 2 && c[i]
        p = permutation[i]
        permutation[i] = permutation[k]
        permutation[k] = p
        ++c[i]
        i = 1
        result.push(permutation.slice())
      } else {
        c[i] = 0
        ++i
      }
    }
    return result
  },

  getSelectorCombinations (selector) {
    var combinations = []
    var prefix = selector.tags.length ? selector.tags[0] : ''
    var suffix = selector.pseudoClasses.length ? ':' + selector.pseudoClasses.join(':') : ''
    var middleOptions = []
    middleOptions = middleOptions.concat(selector.classes.map((value) => '.' + value))
    middleOptions = middleOptions.concat(selector.ids.map((value) => '#' + value))
    middleOptions = middleOptions.concat(selector.attrs.map(([key, value]) => '[' + key + '=' + value + ']'))
    var permutations = this.permute(middleOptions)
    permutations.forEach(function (permutation) {
      combinations.push(prefix + permutation.join('') + suffix)
    })
    return combinations
  },

  getSimilarSelector (selector, ruleSets) {
    var parsedSelector = this.parseCssSelectorString(selector)
    var combinations = this.getSelectorCombinations(parsedSelector)
    for (var i = 0; i < combinations.length; i++) {
      if (ruleSets[combinations[i]]) {
        selector = combinations[i]
        break
      }
    }
    return selector
  },

  getOuterHeight (element) {
    var style = window.getComputedStyle(element)
    var height = element.clientHeight
    var margin = parseFloat(style.marginLeft) + parseFloat(style.marginRight)
    return height + margin
  }

}
