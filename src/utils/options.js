import utils from './index.js'

// Elements
import Section from '../elements/Section.vue'
import Columns from '../elements/Columns.vue'
import Column from '../elements/Column.vue'
import RichText from '../elements/RichText.vue'
import LinkElement from '../elements/LinkElement.vue'
import TextTag from '../elements/TextTag.vue'
import ImageElement from '../elements/ImageElement.vue'
import DivElement from '../elements/DivElement.vue'
import Raw from '../elements/Raw.vue'

const icons = require('feather-icons').icons

export default {
  previewTarget: '#content',
  elementTypes: {
    'rich-text-element': RichText,
    'section-element': Section,
    'columns': Columns,
    'column': Column,
    'link-element': LinkElement,
    'text-tag': TextTag,
    'image-element': ImageElement,
    'div-element': DivElement,
    'raw': Raw
  },
  saveUrl: '/save',
  gridColumns: 12,
  gridColumnName: 'col-md-',
  gridSizes: utils.getGridPercentages(12),
  formOptions: {
    validateAfterLoad: true,
    validateAfterChanged: true
  },
  draggableOptions: {
    group: 'elements',
    dragoverBubble: true
  },
  draggableStart: function (event) {
    setTimeout(function () {
      event.clone.style.display = ''
      event.clone.style.opacity = '.5'
    }, 20)
  },
  mediaQueries: [
    {
      width: '468px',
      query: '(max-width: 468px)',
      name: 'Mobile',
      icon: icons.smartphone.toSvg(),
      order: 2
    },
    {
      width: '768px',
      query: '(max-width: 768px)',
      name: 'Tablet',
      icon: icons.tablet.toSvg(),
      order: 3
    },
    {
      width: '100%',
      name: 'Desktop',
      icon: icons.monitor.toSvg(),
      default: true
    }
  ],
  deleteAssetUrl: '/delete_asset'
}
