# Nimble Page Builder

A page builder for visually creating semantic HTML and CSS3 pages without the bloat.

* CSS importing and extraction  
* HTML importing and semantic output
* Back-end agnostic
* Extensible widgets and blocks

![Nimble Page Builder](static/images/page-builder.jpg)

## Installing

To get Nimble up and running, include `dist/nimble.js` in your page:

Then initialize it by calling the `init` function with the selector on your page and pass in any config options.

``` javascript
var config = {}
Nimble.default.init('#app', config);
```

To preload the builder with HTML or CSS, you can use `setHtml` with any HTML string or CSS string passed as a parameter.

``` javascript
Nimble.default.setHtml('<div class="my-div">This class and text will be parsed by the builder.</div>');
Nimble.default.setCss('.my-div { background-color: blue; }');
```

### Configuration options

``` json
previewTarget: '#content',
saveUrl: '/save',
gridColumns: 12,
gridColumnName: 'col-md-',
gridSizes: utils.getGridPercentages(12),
formOptions: {
  validateAfterLoad: true,
  validateAfterChanged: true
},
draggableOptions: {
  group: 'elements',
  dragoverBubble: true
},
draggableStart: function (event) {
  setTimeout(function () {
    event.clone.style.display = ''
    event.clone.style.opacity = '.5'
  }, 20)
},
mediaQueries: [
  {
    width: '468px',
    query: '(max-width: 468px)',
    name: 'Mobile',
    icon: Vue.prototype.$constants.feather.smartphone.toSvg(),
    order: 2
  },
  {
    width: '768px',
    query: '(max-width: 768px)',
    name: 'Tablet',
    icon: Vue.prototype.$constants.feather.tablet.toSvg(),
    order: 3
  },
  {
    width: '100%',
    name: 'Desktop',
    icon: Vue.prototype.$constants.feather.monitor.toSvg(),
    default: true
  }
],
deleteAssetUrl: '/delete_asset'
```

## Development

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

### Adding widgets

To add in your own custom widgets, call the `addWidgets` method before initialization with an array of widgets.

``` javascript
Nimble.default.addWidgets([teamMemberWidget]);
```

Widgets take the follow properties:

* __render()__ : A function to render the element in Vue
* __props__ : Any accepted props.  By default this should a property `model` of type Object.
* __name__ : The slug for the widget
* __label__ : The human readable label for the widget
* __groups__ : An array of fields for input
* __parseNode(node)__ : A function to determine whether or not a node will be of this widget type on import.  Return any non-null value to be true or optionally return properties for this model.

For a full list of fields available, see
https://icebob.gitbooks.io/vueformgenerator/content/