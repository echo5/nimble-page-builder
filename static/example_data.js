window.html = `
  <div class="hero">
    <div class="container">
      <h1 class="hero-text">Nimble Page Builder</h1>
      <p>Better page building.  Better HTML &amp; CSS.</p>
    </div>
  </div>
  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <img src="/static/images/0093-cloud-database.png">
          <h3>Back-end agnostic</h3>
          <p>Use Nimble with your favorite back-end.  Data can be stored in any database and parsed as HTML.</p>
        </div>
        <div class="col-md-4">
          <img src="/static/images/0780-code.png">
          <h3>Semantic code</h3>
          <p>Clean code output without the tag soup.  CSS is also extracted via classes, so no inline styling.</p>
        </div>
        <div class="col-md-4">
          <img src="/static/images/0115-cog.png">
          <h3>Add your own widgets</h3>
          <p>Easily add your own widgets through JavaScript with customizable fieldsets.</p>
        </div>
      </div>
    </div>
  </div>
  `
window.css = `
  body {
    font-size: 20px;
    font-family: Helvetica, Arial, sans-serif;
  }
  .hero {
    background-image: url('/static/images/bg.jpeg');
    background-size: cover;
    padding-top: 10em;
    padding-bottom: 10em;
  }
  .hero p {
    font-size: 24px;
  }
  .container {
    max-width: 1140px;
    margin: 0 auto;
  }
  .col-md-4 {
    float: left;
    width: 33.33%;
  }
  h1 {
    font-size: 48px;
  }
  `
